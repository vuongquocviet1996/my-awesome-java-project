package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UpZipFile {
	public static void main(String[] args){
		byte[] buffer = new byte[1024];
		String path = "./file/zipFile.zip";

		ZipInputStream zipInputStream;
		ZipEntry zipEntry;

		String folderPath = path.substring(0, path.length() - 4);
		try {
			//Create output directory if not exists
			File folder = new File(path);
			if (!folder.exists()) {
				folder.mkdir();
			}

			zipInputStream = new ZipInputStream(new FileInputStream(path));
			zipEntry = zipInputStream.getNextEntry();

			while (zipEntry != null) {
				String fileName = zipEntry.getName();
				System.out.println("File: "+ fileName);
				String fileSeparator = System.getProperty("file.separator", "/");
				fileName = fileName.replace("\\", fileSeparator);
				File newFile = new File(folderPath.concat(System.getProperty("file.separator", "/")).concat(fileName));

				//create all non exists folders
				//else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();
				System.out.println("File: "+ fileName);

				zipEntry = zipInputStream.getNextEntry();
			}
			zipInputStream.closeEntry();
			zipInputStream.close();

			System.out.println("Finish extract zip file");
		} catch (Exception ex) {
			System.out.println("Extract zip file error :" + ex.getMessage());
		}
	}
}
